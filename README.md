# Computer Programming for Musicians CA2

Michael Grinnell

Minor Project CA2 

Complete 9 tasks provided

Task 1: Amend P011 so that it asks you for a MIDI note number and then prints a Major Scale of
MIDI note numbers to screen starting with that number. Make use of a for loop and array in your
answer.

Task 2: Amend P021 so that the playback is random and there’s a 50% probability of Middle C (MIDI
note number 60) being played and a 25% proability of the E above Middle C (MIDI note number 64)
being played.

Task 3: Amend Row Row Row your Boat so that it instead plays the 8-bar tune Morning is Come.
Here are the Csound pitches for Morning is Come: 8.07, 8.09, 8.06, 8.07, 8.11, 9.00, 8.09, 8.11, 9.02,
9.02, 9.02, 9.02, 9.00, 8.11, 8.09, 8.07, 8.02, 8.02, 8.02, 8.07. Here are the matching duration
numbers for those pitches: 1, 1, 1, 3, 1, 1, 1, 3, 1, 1, 1, 1, .5, .5, .5, .5, 1, 1, 1, 3. Use Google if you
would like to see the sheet music for Morning is Come.

Task 4: Amend your answer to Task 3 to include a basic harmony line (one harmony note for each of
the 8 bars).

Task 5: Amend your answer to Task 3 so that it plays Morning is Come as a 4-part round.

Task 6: Amend P021 so that the playback is random. In addition, allow the user to specify the length
of the tune (i.e. the number of notes) and to select from 3 tempos.

Task 7: Amend P022 (the Rhythmic Motives program) to work with 3/4 rather than 4/4.

Task 8: Amend the Moving Accent program to allow the user to specify where the accent comes.

Task 9: Take ANY ONE of your previous answers and implement it as a multifunction program.