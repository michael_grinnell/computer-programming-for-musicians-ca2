#pragma once

#include "Utility.hpp"
#include "csound.hpp"
#include <string>
#include <vector>

class CsoundHelper
{
public:
	CsoundHelper();
	~CsoundHelper();

	void Reset();

	void SetScore(unsigned short length);
	void SetScore(unsigned short length, unsigned short rounds);
	void SetHarmonyScore(unsigned short i);
	void SetScoreString(std::string score);
	void PlayScore();

	void SetTempo(float tempo);

	void SetMidiNotes(std::vector<unsigned short> midiNotes);
	void SetVolume();
	void SetDurations(std::vector<float> durations);
	void SetStartTimes(std::vector<float> startTimes);
	void SetPitches(std::vector<float> pitches);
	void SetHarmonyNotes(std::vector<float> harmonyPitches);
private:
	float m_tempo;

	std::string m_orc;
	std::string m_sco;
	Csound* m_csound;

	std::vector<unsigned short> m_midiNotes;

	std::vector<float> m_durations;
	std::vector<float> m_startTimes;
	std::vector<float> m_pitches;
	std::vector<float> m_harmonyPitches;
};

