#pragma once
#include <iostream>
#include <string>

bool ValidChoice(char* input, unsigned short max);
int GetNumericInput(const char* message, int min, int max);
bool StringReplace(std::string& str, const std::string& from, const std::string& to);
void PrintScale(const float* scale);

