#pragma once
#include "GUI.hpp"
#include "Tasks.hpp"

class Application
{
	public:
		Application();
		void Run();
		void RunTask(unsigned short task);
private:
	GUI m_GUI;
	Tasks m_tasks;
};

