#pragma once
#include <string>

const int NOTES_IN_OCTAVE = 12;
const std::string NOTES[] = { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" };

const float WHOLE_TONE_SCALE[] = { 8.00,8.02,8.04,8.06,8.08,8.10,9.00 };
const float MAJOR_SCALE[] = { 8.00,8.02,8.04,8.05,8.07,8.09,8.11,9.00 };

const int MAJOR_SCALE_INT[] = { 0,2,4,5,7,9,11,12};


const std::vector<float> MHC_PITCHES =
{
	8.07f, 8.09f, 8.06f, 8.07f,
	8.11f, 9.00f, 8.09f, 8.11f,
	9.02f, 9.02f, 9.02f, 9.02f,
	9.00f, 8.11f, 8.09f, 8.07f,
	8.02f, 8.02f, 8.02f, 8.07f
};

const std::vector<float> MHC_DURATIONS =
{
	1.f, 1.f, 1.f, 3.f,
	1.f, 1.f, 1.f, 3.f,
	1.f, 1.f, 1.f, 1.f,
	.5f, .5f, .5f, .5f,
	1.f, 1.f, 1.f, 3.f
};

const std::vector<float> MHC_HARMONYS =
{
	7.07f, 7.07f, 7.11f, 7.11f,
	8.02f, 8.02f, 7.02f, 7.07f
};
