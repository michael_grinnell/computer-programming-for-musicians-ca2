#pragma once
#include "CsoundHelper.hpp"

class Tasks
{
public:
	Tasks();

	void RunTask(unsigned short taskNum);

private:
	void Task1();
	void Task2();
	void Task3();
	void Task4();
	void Task5();
	void Task6();
	void Task7();
	void Task8();
	void Task9();

private:
	CsoundHelper m_csoundHelper;
};
