#include "CsoundHelper.hpp"
#include <iostream>


CsoundHelper::CsoundHelper() 
	: m_csound(new Csound)
{
	Reset();
}

CsoundHelper::~CsoundHelper()
{
	delete m_csound;
}

void CsoundHelper::Reset()
{
	m_csound->Reset();
	csoundSetGlobalEnv("OPCODE6DIR64", "C:\\Program Files\\Csound6_x64\\plugins64");
	m_csound->SetOption("-odac");

	m_orc = ("sr=44100\n\
    ksmps=32\n\
    nchnls=2\n\
    0dbfs=1\n\
    \n\
    instr 1\n\
    aEnv linen 1, .1, p3, .1\n\
    aout oscili 0.25*aEnv, cpsmidinn(p4)\n\
    outs aout, aout\n\
    endin");

	m_sco = "";
	m_tempo = 1;

	m_durations.clear();
	m_midiNotes.clear();
	m_startTimes.clear();
	m_pitches.clear();
	m_harmonyPitches.clear();
}

void CsoundHelper::SetHarmonyNotes(std::vector<float> harmonyPitches)
{
	m_harmonyPitches = harmonyPitches;
}

void CsoundHelper::SetDurations(std::vector<float> durations)
{
	m_durations = durations;
}

void CsoundHelper::SetMidiNotes(std::vector<unsigned short> midiNotes)
{
	StringReplace(m_orc, "cpspch(p4)", "cpsmidinn(p4)");
	m_midiNotes = midiNotes;
}

void CsoundHelper::SetVolume()
{
	if(!m_pitches.empty())
		StringReplace(m_orc, "0.25*aEnv, cpspch(p4)", "p4*aEnv, cpspch(p5)");
	else
		StringReplace(m_orc, "0.25*aEnv, cpsmidinn(p4)", "p4*aEnv, cpspch(p5)");
	
}

void CsoundHelper::SetStartTimes(std::vector<float> startTimes)
{
	m_startTimes = startTimes;
}

void CsoundHelper::SetPitches(std::vector<float> pitches)
{
	StringReplace(m_orc, "cpsmidinn(p4)", "cpspch(p4)");
	m_pitches = pitches;
}

void CsoundHelper::SetScore(unsigned short length)
{

	float start;
	float duration;
	std::string note;
	for (unsigned short i = 0; i < length; i++)
	{
		start = m_startTimes.empty() ? i : m_startTimes[i];
		duration = m_durations.empty() ? 1 : m_durations[i];
		note = m_pitches.empty() ? std::to_string(m_midiNotes[i]) : std::to_string(m_pitches[i]);

		m_sco.append("i1 " + std::to_string(start * m_tempo) + " " + std::to_string(duration * m_tempo) + " " + note + "\n");

		if (!m_harmonyPitches.empty() && !((int)start % 3))
			m_sco.append("i1 " + std::to_string(start) + " " + "3" + " " + std::to_string(m_harmonyPitches[i / 3]) + "\n");
	}

}

void CsoundHelper::SetScore(unsigned short length, unsigned short rounds)
{
	float start;
	float roundStart = 6;
	float duration;
	std::string note;

	for (size_t i = 0; i < rounds; ++i)
	{
		for (unsigned short j = 0; j < length; ++j)
		{
			start = m_startTimes.empty() ? j : m_startTimes[j];
			duration = m_durations.empty() ? 1 : m_durations[j];
			note = m_pitches.empty() ? std::to_string(m_midiNotes[j]) : std::to_string(m_pitches[j]);

			m_sco.append("i1 " + std::to_string(start + (roundStart * i)) + " " + std::to_string(duration) + " " + note + "\n");
		}
	}
}

void CsoundHelper::SetScoreString(std::string score)
{
	m_sco = score;
}

void CsoundHelper::PlayScore()
{
	std::cout << m_sco.c_str();
	
	//compile orc
	m_csound->CompileOrc(m_orc.c_str());

	//compile sco
	m_csound->ReadScore(m_sco.c_str());

	//prepare Csound for performance
	m_csound->Start();

	//perform entire score
	m_csound->Perform();

	Reset();
}

void CsoundHelper::SetTempo(float tempo)
{
	m_tempo = tempo;
}
