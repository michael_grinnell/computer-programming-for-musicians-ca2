#include "Tasks.hpp"
#include "Utility.hpp"
#include "Constants.hpp"


#include <iostream>
#include <stdio.h>
#include <string>
#include <time.h>

Tasks::Tasks()
{
}

void Tasks::RunTask(unsigned short taskNum)
{
	switch (taskNum)
	{
	case 1:
		Task1();
		break;
	case 2:
		Task2();
		break;
	case 3:
		Task3();
		break;
	case 4:
		Task4();
		break;
	case 5:
		Task5();
		break;
	case 6:
		Task6();
		break;
	case 7:
		Task7();
		break;
	case 8:
		Task8();
		break;
	case 9:
		Task9();
		break;
	default:
		break;
	}
}

//Task 1: Amend P011 so that it asks you for a MIDI note number and then prints a Major Scale of
//MIDI note numbers to screen starting with that number.Make use of a for loopand array in your
//answer.

//Task one gets input from the user using the cin function, it is then validated to ensure it is a number between 0 and 127. 
//It then iterates through the major scale array using a for loop.It sets the output to be equal to the input + the current index of the major scale.
//If the output becomes greater than 127 it wraps around back to one.Each iteration the output is printed to screen using cout.

void Tasks::Task1()
{
	int output;
	int number = GetNumericInput("\tEnter a valid MIDI note number (0-127) and press Return.\n\n", 0, 127);
	int length = sizeof(MAJOR_SCALE_INT) / sizeof(int);

	std::cout << "\n\tOUTPUT: ";

	for (size_t i = 0; i < length; i++)
	{
		output = number + MAJOR_SCALE_INT[i];

		if (output > 127)
			output -= 127;

		std::cout << output;

		if(i < length-1)
			std::cout << ", ";
	}

	std::cout << std::endl;
}

//Task 2: Amend P021 so that the playback is random and there�s a 50 % probability of Middle C(MIDI
//note number 60) being played and a 25 % proability of the E above Middle C(MIDI note number 64)
//being played.

//First middle cand the e above are removed from the midi scale array.
//Then a random number between 0 - 99 is chosen.If the number is less than 75 i.e.a 75 % chance of being chosen that
//means the output will either be middle c or the e above.This number is then checked to see if it is less than 25, 
//if it is it midi note 64 is assigned, if its not midi note 60 is assigned.
//Otherwise a random number is selected from the midi scale.These notes are then added to the new scale for playback.

void Tasks::Task2()
{
	//remove 60, 64 from scale for accurate probability
	std::vector<unsigned short> midiscale = { 62,65,67,69,71,72 };
	std::vector<unsigned short> newScale;

	for (size_t i = 0; i < 8; ++i)
	{
		int random = rand() % 100;
		int midiNote;

		if (random < 75)
			if (random < 25)
				midiNote = 64;
			else
				midiNote = 60;
		else
			midiNote = midiscale[rand() % midiscale.size()];

		newScale.push_back(midiNote);
	}

	m_csoundHelper.SetMidiNotes(newScale);
	m_csoundHelper.SetScore(newScale.size());
	m_csoundHelper.PlayScore();
}

//Task 3: Amend Row Row Row your Boat so that it instead plays the 8 - bar tune Morning is Come.
//Here are the Csound pitches for Morning is Come : 8.07, 8.09, 8.06, 8.07, 8.11, 9.00, 8.09, 8.11, 9.02,
//9.02, 9.02, 9.02, 9.00, 8.11, 8.09, 8.07, 8.02, 8.02, 8.02, 8.07.Here are the matching duration
//numbers for those pitches : 1, 1, 1, 3, 1, 1, 1, 3, 1, 1, 1, 1, .5, .5, .5, .5, 1, 1, 1, 3. Use Google if you
//would like to see the sheet music for Morning is Come.

//The pitches for morning has come as well as the durations are assigned to the pitches and durations arrays respectively for playback.
void Tasks::Task3()
{
	int length = MHC_PITCHES.size();
	std::vector<float> startTimes;
	float start = 0;

	for (size_t i = 0; i < length; ++i)
	{
		startTimes.push_back(start);
		start += MHC_DURATIONS[i];
	}
		
	m_csoundHelper.SetPitches(MHC_PITCHES);
	m_csoundHelper.SetDurations(MHC_DURATIONS);
	m_csoundHelper.SetStartTimes(startTimes);
	m_csoundHelper.SetScore(length);
	m_csoundHelper.PlayScore();
}
//Task 4: Amend your answer to Task 3 to include a basic harmony line (one harmony note for each of
//the 8 bars).

//Along with the pitches and durations arrays, a third array is added containing the harmony notes for morning has come.
//The duration of each harmony note is set to the length of 1 bar, whenever we reach the end of a bar the next harmony note is inserted into the melody.

void Tasks::Task4()
{
	int length = MHC_PITCHES.size();
	std::vector<float> startTimes;
	float start = 0;

	for (size_t i = 0; i < length; ++i)
	{
		startTimes.push_back(start);
		start += MHC_DURATIONS[i];
	}

	m_csoundHelper.SetPitches(MHC_PITCHES);
	m_csoundHelper.SetHarmonyNotes(MHC_HARMONYS);
	m_csoundHelper.SetDurations(MHC_DURATIONS);
	m_csoundHelper.SetStartTimes(startTimes);
	m_csoundHelper.SetScore(length);
	m_csoundHelper.PlayScore();
}
//Task 5: Amend your answer to Task 3 so that it plays Morning is Come as a 4-part round. 
//This will play morning has come 4 times, the same as task 3, however each iteration of the loop the start times will be offset by one bar.
void Tasks::Task5()
{
	int length = MHC_PITCHES.size();
	std::vector<float> startTimes;
	float start = 0;

	for (size_t i = 0; i < length; ++i)
	{
		startTimes.push_back(start);
		start += MHC_DURATIONS[i];
	}

	m_csoundHelper.SetPitches(MHC_PITCHES);
	m_csoundHelper.SetHarmonyNotes(MHC_HARMONYS);
	m_csoundHelper.SetDurations(MHC_DURATIONS);
	m_csoundHelper.SetStartTimes(startTimes);
	m_csoundHelper.SetScore(length, 4);
	m_csoundHelper.PlayScore();
}

//Task 6: Amend P021 so that the playback is random. In addition, allow the user to specify the length
//of the tune(i.e.the number of notes) and to select from 3 tempos.

//The user input is used to specify how many notes will be added to the midi notes array.
//Each iteration of the loop a random value from the midi scale is selectedand added to the new scale.
//The user also specifies the tempo of the playback, depending on the input a different number is assigned to the speed variable.
//This is then used to alter the playback rate by multiplying by the start timeand duration values.I.e.twice as fast will be multiplied by 0.5.
void Tasks::Task6()
{
	srand(time(0));

	std::vector<unsigned short> midiscale = { 60,62,64,65,67,69,71,72 };
	std::vector<unsigned short> newScale;
	float speed;

	int notes = GetNumericInput("\n\tPlease Input the number of Notes: \n\n", 0, 1000);
	int tempoChoice = GetNumericInput("\n\tPlease Choose a Tempo\n\t(1)Half Speed, (2)Normal Speed, (3)X2 Speed, (4)X4 Speed\n", 1, 4);

	std::cout << "NOTES: " << notes << "\tTempo: " << tempoChoice << std::endl;

	switch (tempoChoice)
	{
	case 1:
		speed = 2;
		break;
	case 2:
		speed = 1;
		break;
	case 3:
		speed = 0.5f;
		break;
	case 4:
		speed = 0.25f;
		break;
	default:
		speed = 1;
		break;
	}

	for (size_t i = 0; i < notes; ++i)
		newScale.push_back(midiscale[rand() % midiscale.size()]);
	
	m_csoundHelper.SetMidiNotes(newScale);
	m_csoundHelper.SetTempo(speed);
	m_csoundHelper.SetScore(newScale.size());
	m_csoundHelper.PlayScore();
}

//Task 7: Amend P022 (the Rhythmic Motives program) to work with 3/4 rather than 4/4. 

//The length of one bar is set to 3, as well as this the durations in the durations array will add up to a total of 12 as this is evenly divisible by 3.
//The first iteration of each bar the duration is set to a random value from the durations array.This random value is restricted to being an index divisible by 3, 
//this ensures that the sum of the durations in each bar is equal to 3.
void Tasks::Task7()
{
	int note, dur, start = 0;
	float durations[12] = { 0.5f,0.5f,2.f,1,1,1,3,2,0.25f,0.25f,0.5f};
	int dursum = 0,  bars;
	int motivestart = 0;

	std::cout << "How many bars would you like in the piece?\n";
	std::cin >> bars;
	std::string score;

	while (dursum < (bars * 3))
	{
		dursum % 3 ? motivestart++ : motivestart = (rand() % 4) * 3;
		dur = durations[motivestart];

		if (dursum == (bars * 3) - 3)
			dur = 3;

		score.append("i1 " + std::to_string(start * .2) + " " + std::to_string(dur * .2) + " " + std::to_string(60) + "\n");

		start += dur;
		dursum += dur;
	}

	m_csoundHelper.SetScoreString(score);
	m_csoundHelper.PlayScore();
}

//Task 8: Amend the Moving Accent program to allow the user to specify where the accent comes. 

//The user specifies the step size of the moving accent.
//I.e. if the user choses a step size of 2, the accent will be on the 2nd note of the first bar, 
//then the 4th, note of the 2nd bar and so on.
//Onces the accent is greater than the notes in the bar it will start again.
//The accent is essentially done by setting the amplitude of the note to be higher than that of the rest.
//The note number is checked against the bar value and the accent offset to determine if the amplitude should be sit to 1.
void Tasks::Task8()
{
	float vol, start = 0;
	std::string sco = "";
	int barSize = 8;
	int bar = 0;
	int note = 8;
	int offset;

	std::cout << "Please Specify The Step Size for the Moving Accent\nSize: ";
	std::cin >> offset;

	for (size_t i = 0; i < 64; ++i)
	{
		note += (i % barSize == 0) ? -7 : 1;

		if (i % barSize == 0)
			bar += (bar * offset < barSize) ? 1 : -bar;

		vol = (note == bar * offset) ? 1 : 0.2f;

		sco.append("i1 " + std::to_string(start) + " " + std::to_string(.2) + " " + std::to_string(vol) + " " + std::to_string(8.00) + "\n");
		start += .2f;
	}

	m_csoundHelper.SetVolume();
	m_csoundHelper.SetScoreString(sco);
	m_csoundHelper.PlayScore();
}

void Tasks::Task9()
{
	std::cout << "Application is a multifunction program" << std::endl;
}
