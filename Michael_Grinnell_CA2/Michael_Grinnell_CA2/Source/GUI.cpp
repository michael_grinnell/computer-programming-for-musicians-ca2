﻿#include "GUI.hpp"
#include <stdio.h>
#include <iostream>

GUI::GUI() {}

void GUI::PrintMain()
{
	std::cout << "\n\t-------- Michael Grinnell CA2 --------\n" << std::endl;
	std::cout << "\tPlease enter the number of the task you wish to run\n\n\t(0) To exit Application" << std::endl;
}

void GUI::PrintQuestion(unsigned short question)
{
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
	switch (question)
	{
		
	case 0:
		std::cout << "\tGoodbye" << std::endl;
		break;

	case 1:
		std::cout << "\tTask 1: Amend P011 so that it asks you for a MIDI note number and then prints a Major Scale of\n"
					"\tMIDI note numbers to screen starting with that number. Make use of a for loop and array in your answer. " << std::endl;
		break;

	case 2:
		std::cout << "\tTask 2: Amend P021 so that the playback is random and there\'s a 50% probability of Middle C (MIDI\n"
			"\tnote number 60) being playedand a 25 % proability of the E above Middle C(MIDI note number 64)\n"
			"\tbeing played. " << std::endl;
		break;

	case 3:
		std::cout << "\tTask 3: Amend Row Row Row your Boat so that it instead plays the 8-bar tune Morning is Come.\n"
			"\tHere are the Csound pitches for Morning is Come : 8.07, 8.09, 8.06, 8.07, 8.11, 9.00, 8.09, 8.11, 9.02,\n"
			"\t9.02, 9.02, 9.02, 9.00, 8.11, 8.09, 8.07, 8.02, 8.02, 8.02, 8.07.Here are the matching duration\n"
			"\tnumbers for those pitches : 1, 1, 1, 3, 1, 1, 1, 3, 1, 1, 1, 1, .5, .5, .5, .5, 1, 1, 1, 3. Use Google if you\n"
			"\twould like to see the sheet music for Morning is Come. " << std::endl;
		break;

	case 4:
		std::cout << "\tTask 4: Amend your answer to Task 3 to include a basic harmony line (one harmony note for each of\n"
			"\tthe 8 bars). " << std::endl;
		break;

	case 5:
		std::cout << "\tTask 5: Amend your answer to Task 3 so that it plays Morning is Come as a 4-part round. " << std::endl;
		break;

	case 6:
		std::cout << "\tTask 6: Amend P021 so that the playback is random. In addition, allow the user to specify the length\n"
			"\tof the tune(i.e.the number of notes) and to select from 3 tempos. " << std::endl;
		break;

	case 7:
		std::cout << "\tTask 7: Amend P022 (the Rhythmic Motives program) to work with 3/4 rather than 4/4. " << std::endl;
		break;

	case 8:
		std::cout << "\tTask 8: Amend the Moving Accent program to allow the user to specify where the accent comes. " << std::endl;
		break;

	case 9:
		std::cout << "\tTask 9: Take ANY ONE of your previous answers and implement it as a multifunction program." << std::endl;
		break;

	default:
		break;
	}
	std::cout << "\t___________________________________________________________________________________________\n" << std::endl;
}

