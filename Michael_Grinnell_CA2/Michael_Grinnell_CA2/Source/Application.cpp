#include "Application.hpp"
#include "Tasks.hpp"

#include <iostream>
#include "Utility.hpp"
#include <time.h>
Application::Application()
{
	//Seeds Rand when onject is created
	srand(time(NULL));
}

void Application::Run()
{
	//Print Main Menu
	m_GUI.PrintMain();

	//Get User Input
	char input[1];
	std::cout << "\n\tINPUT: ";
	std::cin >> input;

	//Input Valdisation
	if (!ValidChoice(input, 15))
	{
		std::cout << "\nPlease Enter A valid Choice\n" << std::endl;
		Run();
	}
	
	unsigned short choice = strtol(input, NULL, 10);

	m_GUI.PrintQuestion(choice);

	if (choice == 0)
		exit(0);

	m_tasks.RunTask(choice);

	std::cout << "\n\n" << std::endl;
	system("pause");
	Run();
}
