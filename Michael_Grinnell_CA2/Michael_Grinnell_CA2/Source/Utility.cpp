#include "Utility.hpp"

bool ValidChoice(char* input, unsigned short max)
{
	if (input[strspn(input, "0123456789")] == 0)
		if (strtol(input, NULL, 10) <= max)
			return true;

	return false;
}

int GetNumericInput(const char* message, int min, int max)
{
	bool isValid = false;
	bool isNumber = false;
	bool isInRange = false;

	int input;

	while (!isValid)
	{
		std::cout << message;
		std::cout << "\tINPUT: ";
		std::cin >> input;

		if (std::cin && !(input < min || input > max))
			isValid = true;
		else
		{
			std::cout << "\n\tInvalid Input!\n ";
			std::cin.clear();
			std::cin.ignore();
		}
	}

	return input;
}

//https://stackoverflow.com/questions/3418231/replace-part-of-a-string-with-another-string/3418285
bool StringReplace(std::string& str, const std::string& from, const std::string& to)
{
	size_t start_pos = str.find(from);
	if (start_pos == std::string::npos)
		return false;
	str.replace(start_pos, from.length(), to);
	return true;
}

void PrintScale(const float* scale)
{
	float start = 0;
	std::cout << "f1\t0\t4096\t10\t1\n\n";

	for (int i = 0; i < 7; ++i)
	{
		std::cout << "i1\t" << start << "\t.5\t32000\t" << scale[i] << "\n";
		start += 0.5f;
	}

	std::cout << "e";
}